import logging
from typing import override

str_format = "[%(asctime)s][%(levelname)s]{%(name)s:%(lineno)d} - %(message)s"
date_format = "%Y/%m/%d %I:%M:%S %p"


logger = logging.getLogger()
logger.setLevel(logging.DEBUG)


class ColorFormatter(logging.Formatter):
    # https://stackoverflow.com/questions/384076/how-can-i-color-python-logging-output
    grey = "\x1b[38;20m"
    yellow = "\x1b[33;20m"
    blue = "\x1b[34;20m"
    red = "\x1b[31;20m"
    bold_red = "\x1b[31;1m"
    reset = "\x1b[0m"

    FORMATS = {
        logging.DEBUG: grey + str_format + reset,
        logging.INFO: blue + str_format + reset,
        logging.WARNING: yellow + str_format + reset,
        logging.ERROR: red + str_format + reset,
        logging.CRITICAL: bold_red + str_format + reset,
    }

    def format(self, record):
        log_fmt = self.FORMATS.get(record.levelno)
        formatter = logging.Formatter(log_fmt, date_format)
        return formatter.format(record)


class ErrorHandler(logging.Handler):
    fired = False

    @override
    def emit(self, record):
        self.fired = True


# create file handler which logs even debug messages
filename = ".log"
file_handler = logging.FileHandler(filename)
file_handler.setLevel(logging.DEBUG)

# create console handler with a higher log level
console_handler = logging.StreamHandler()
console_handler.setLevel(logging.INFO)

# create error handler
error_handler = ErrorHandler()
error_handler.setLevel(logging.ERROR)

# create formatter and add it to the handlers
formatter = logging.Formatter(str_format, date_format)
console_handler.setFormatter(ColorFormatter())
file_handler.setFormatter(formatter)

# add the handlers to logger
logger.addHandler(file_handler)
logger.addHandler(console_handler)
logger.addHandler(error_handler)
