import os

from dotenv import load_dotenv

load_dotenv(override=True)


ATLASSIAN_USR = os.environ["ATLASSIAN_USR"]
ATLASSIAN_PWD = os.environ["ATLASSIAN_PWD"]
ATLASSIAN_URL = os.environ["ATLASSIAN_URL"]
