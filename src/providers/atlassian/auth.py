import requests
from requests.auth import HTTPBasicAuth

from src import vars


class AtlassianAuth:
    AUTH = HTTPBasicAuth(vars.ATLASSIAN_USR, vars.ATLASSIAN_PWD)

    @classmethod
    def request(cls, method, path, *args, **kwargs) -> requests.Response:
        kwargs.setdefault("headers", {})
        kwargs.setdefault("auth", cls.AUTH)
        kwargs["headers"] |= {"X-Atlassian-Token": "no-check"}
        url = f"{vars.ATLASSIAN_URL}/rest/api{path}"
        return requests.request(method, url, *args, **kwargs)
