from pydantic import BaseModel


class PageId(BaseModel):
    id: int


class Page(PageId):
    type: str
    status: str
    title: str
