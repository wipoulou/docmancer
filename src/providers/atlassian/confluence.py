import logging
from pprint import pprint

from src.providers.atlassian.auth import AtlassianAuth
from src.providers.atlassian.page import Page, PageId

logger = logging.getLogger(__name__)


class ConfluenceV1(AtlassianAuth):
    @classmethod
    def list_pages(cls, start: int = 0, limit: int = 25) -> list[PageId]:
        logger.info(f"Listing pages from {start} to {start+limit}")
        r = cls.request("GET", f"/content?start={start}&limit={limit}")
        r.raise_for_status()
        print(r.json().get("start"))
        print(r.json().get("limit"))
        print(r.json().get("size"))
        return [Page(**result) for result in r.json().get("results", [])]

    @classmethod
    def get_page_children(cls, page: Page | PageId) -> list[PageId]:
        r = cls.request("GET", f"/content/{page.id}/descendant/attachement")
        r.raise_for_status()
        pprint(r.json())
        return [PageId(result.get("id")) for result in r.json().get("attachment").get("results", [])]

    @classmethod
    def get_page(cls, page: PageId) -> Page:
        r = cls.request("GET", f"/content/{page.id}", params={"expand": "children.page"})
        r.raise_for_status()
        import json

        logger.debug(json.dumps(json.loads(r.text), sort_keys=True, indent=4, separators=(",", ": ")))
        return Page(**r.json())
