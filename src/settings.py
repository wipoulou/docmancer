import os

from src import vars

DEBUG = os.getenv("DEBUG", "True").lower() == "true"

proxies = {
    "http": os.getenv("HTTP_PROXY"),
    "https": os.getenv("HTTPS_PROXY"),
}
